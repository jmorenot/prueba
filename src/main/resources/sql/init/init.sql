DO $$

begin
    if(select count(*)=0 from vendedor)then
        insert into vendedor(nombre,rut)
        values('James Hamilton','00.000.000-0'),
              ('Lucas James','11.111.111-1'),
              ('Albert Finney','22.222.222-2'),
              ('Carlos Fitz','33.333.333-3');

        insert into vehiculo(marca,modelo,valor)
        values('Renault','Megane',650000),
              ('Kia','Cerato',459000),
              ('Porsche','Cayman',100000),
              ('Audi','A4',320000);

        insert into venta(vendedor_id,vehiculo_id,porcentaje_comision)
        values(1,1,1),
              (1,2,1.5),
              (2,1,0.7),
              (2,1,0.2),
              (2,2,1),
              (2,3,1.4),
              (2,4,0.3),
              (3,1,0.7),
              (3,1,0.7),
              (3,1,0.7),
              (4,1,1),
              (4,1,2),
              (4,2,0.7),
              (4,2,0.9),
              (4,3,3),
              (4,3,0.2),
              (4,4,1.7);
    end if;
end $$;
