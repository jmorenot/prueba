create table if not exists vendedor(
 	vendedor_id serial not null primary key,
 	nombre varchar(20) not null,
 	rut varchar(12) not null unique 
);  

create table if not exists vehiculo( 	
        vehiculo_id serial not null primary key,
        marca varchar(30) not null,
        modelo varchar(10) not null,
        valor int not null );  

create table if not exists venta(
        venta_id serial not null primary key,
        vendedor_id int references vendedor(vendedor_id),
        vehiculo_id int references vehiculo(vehiculo_id),
        porcentaje_comision real not null,
        fecha timestamp not null default current_timestamp
);