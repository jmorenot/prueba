/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.engageme.bd;

import cl.engageme.util.ResourcesUtil;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.postgresql.util.PSQLException;

/**
 *
 * @author engageme
 */
public class Bd {

    private static String bdName = "engageme";
    private final String url = "jdbc:postgresql://localhost/";
    private final String user = "postgres";
    private final String password = "";

    private Connection c = null;

    public Bd() {
        try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager.getConnection(url + bdName, user, password);
            System.out.println("CONECTADO CORRECTAMENTE A BASE DE DATOS");
            createBD();
        } catch (PSQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
        }
    }

    public List<Map<String, Object>> queryForList(String sql) {
        List<Map<String, Object>> resultList = new ArrayList<>();
        Statement stmt = null;
        try {
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                Map<String, Object> row = new LinkedHashMap<>();
                int colNum = rs.getMetaData().getColumnCount();
                for (int colIndex = 1; colIndex <= colNum; colIndex++) {
                    row.put(rs.getMetaData().getColumnName(colIndex), rs.getObject(colIndex));
                }
                resultList.add(row);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return resultList;
    }

    public Map<String, Object> queryForMap(String sql) {
        try {
            return queryForList(sql).get(0);
        } catch (Exception e) {
            return null;
        }
    }

    public void execute(String sql) {
        Statement stmt = null;
        try {
            stmt = c.createStatement();
            stmt.execute(sql);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("ERROR AL EJECUTAR: " + sql);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("ERROR AL EJECUTAR: " + sql);
            }
        }
    }

    private void createBD() {
        execute(ResourcesUtil.getContentFile("sql/init/create.sql"));
        execute(ResourcesUtil.getContentFile("sql/init/init.sql"));
    }

}
