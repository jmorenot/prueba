/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.engageme.programa;

import cl.engageme.bd.Bd;
import cl.engageme.bd.objetos.Venta;

/**
 *
 * @author engageme
 */
public class GestionVentas {

    private static Bd bd = new Bd();

    public static void main(String[] args) {
        test();
    }
    
    private static void test(){
        bd.queryForList("select * from venta").forEach(r->{
            System.out.println("Vendedor: "+r.get("vendedor_id")+ " Vehículo:"+r.get("vehiculo_id"));
        });
    }

    private static void registrarVenta(Venta venta) {

    }

    private static void actualizarVenta(Venta venta) {

    }

    private static void eliminarVenta(Venta venta) {

    }
}
