/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.engageme.programa;

import cl.engageme.bd.Bd;
import cl.engageme.bd.objetos.Vehiculo;

/**
 *
 * @author engageme
 */
public class GestionVehiculos {

    private static Bd bd = new Bd();

    public static void main(String[] args) {
        test();
    }
    
    private static void test(){
        bd.queryForList("select * from vehiculo").forEach(r->{
            System.out.println(r.get("marca")+ " "+r.get("modelo"));
        });
    }

    private static void registrarVehiculo(Vehiculo vehiculo) {

    }

    private static void actualizarVehiculo(Vehiculo vehiculo) {

    }

    private static void eliminarVehiculo(Vehiculo vehiculo) {

    }

}
