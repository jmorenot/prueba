/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.engageme.programa;

import cl.engageme.bd.Bd;
import cl.engageme.util.ResourcesUtil;

/**
 *
 * @author engageme
 */
public class Informes {

    private static Bd bd = new Bd();

    public static void main(String[] args) {
        generarInforme();
    }

    private static void generarInforme() {
        //Crear la consulta sql en src/main/resources/sql/informe.sql
        String sql = ResourcesUtil.getContentFile("sql/informe.sql");
        System.out.println("============");
        System.out.println("INFORME");
        System.out.println("============");
        // Construir a partir de aquí la el contenido (impresión por pantalla) del informe
    }
}
