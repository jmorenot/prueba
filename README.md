## Instrucciones

* ESTRUCTURA DEL PROYECTO
	- El proyecto se estructura en 3 paquetes:
		- bd: Contiene la conexión y creación de la base de datos a utilizar en esta prueba, además de la representación de cada 
		tabla en los objetos correspondientes.
		- programa: Contiene las clases que se usarán para el desarrollo de la prueba, mantenedores y generador de informes
		- util: Contiene las clases útiles para utilizar a nivel de proyecto.

* UTILIZACIÓN DE LA CLASE Bd.java
	- La clase Bd.java se utiliza para establecer conexión, generar y ejecutar consultas sql sobre la base de datos... para fines de esta prueba,
	se utiliza postgreSQL. 
	* Para ejecutar la prueba es necesario crear una base de datos de nombre "engageme"

* DESARROLLO DE LA PRUEBA
	- El proyecto ya tiene pre creadas las clases para el desarrollo de la prueba, cada clase crea un objeto Bd listo para ser utilizado. 
	Cada vez que se ejecuta una de estas clases se valida que existan datos de prueba insertados (probar los metodos test() deberían listar 
	elementos desde la base de datos)


## Casos a desarrollar
* Completar los métodos de registrar, actualizar y eliminar en las clases de gestión de vehículos,vendedores y ventas

* Crear métodos de generación de informes utilizando como base la clase Informes.java (Usar impresiones de pantalla para mostrar elementos, no es necesario dibujar tablas o algo parecido, lo importante son los datos). Se requieren los siguientes informes:

    - Informe de ganancias por cada vendedor en base a un rango de fechas (desde y hasta): desplegar el monto total ganado por cada vendedor
    en base a la comisión ganada por cada venta;

    - Ranking de los vehículos más vendidos: desplegar el modelo, marca y cantidad de unidades vendidas en orden descendente en base a la 
    cantidad de unidades vendidas

    - Ranking de mejores vendedores: Desplegar el nombre y rut de los vendedores en orden descendente en base el monto vendido.

* Por alguna razón la clase Error.java da error al ejecutarse, identifique el o los problemas y realice las correcciones necesarias, si necesita ejecutar cambios sobre la base de datos utilice bd.execute();

En caso de cualquier duda enviar un email a julio.moreno@engagemeapps.com

Enviar proyecto comprimido a julio.moreno@engagemeapps.com
